class EditAccessControl
  
  # コンストラクタ
  def initialize(targetFileName = "./default.conf")
    @targetFile = targetFileName
    # ファイルがあるときはそのまま読み込み
    if File.exists?(@targetFile)
      self.LoadConfigure()
    end
  end
  
  # 指定されたファイルを実際に読み込むメソッド
  def LoadConfigure()
    # 初期化
    file = File.open(@targetFile,"r")
    @configHash = Hash::new
    currentSection = ""
    
    while line = file.gets do
      # 先頭行からスペースを削除
      line = line.gsub(" ","")
      
      # 正規表現一致で分岐
      if /^\[.+\]/ =~ line
        # リポジトリのアクセス制限セクションに変更
        sectionString = line.scan(/^\[.+\]/)
        if sectionString.size == 0
          raise("file format is invalid. null section is found.")
        end
        
        # 取得できたなら、セクションの作成
        currentSection = sectionString[0]
        length = currentSection.size
        name = currentSection[1,length-2]
        currentSection = name
        @configHash[currentSection] = Hash::new
      
      elsif /^#/ =~ line
        # 先頭が # = コメント
        next
      
      else
        # その他
        # 文字数 0 なら飛ばす
        if line.size == 0
          next
        end
        
        # = でパースしてHashに突っ込む
        parsed = line.split("=")
        if parsed.size != 2
          next
        end
        
        # 要素に突っ込む
        @configHash[currentSection][parsed[0]] = parsed[1]
      end
    end
    file.close
  end
  
  # 取り出し
  def GetConfigHash()
    return @configHash
  end
  
  # 保存
  def SaveHash(saveFileName="default.ini",saveTargetData=@configHash)
    fp = open(saveFileName,"w")
    
    # group 優先
    if saveTargetData.key?("groups")
      fp.puts("[groups]")
      saveTargetData["groups"].each{|key,value|
        fp.puts(key + "=" + value)
      }
      saveTargetData.delete("groups")
    end
    
    # 書き込み
    saveTargetData.each{|section,inner|
      fp.puts("[" + section + "]")
      inner.each{|key,value|
        fp.puts(key + "=" + value)
      }
    }
    fp.close
  end
end
