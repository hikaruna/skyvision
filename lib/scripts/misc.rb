# フォントのサイズ別キャッシュ機能のゲッター
class Font
	def self.get size
    @@font_c ||= []
    @@font_c[size] ||= Font.new(size)
	end
end

# 配列の要素をランダムに抽出する
if RUBY_VERSION < '1.9.0'
  class Array
    def choice
      at( rand( size ) )
    end
  end
end


$sin = Array.new(360){|i|Math.sin(Math::PI/180*i)}
$cos = Array.new(360){|i|Math.cos(Math::PI/180*i)}

module Math
	def self.abs(i)
		i>0 ? i : -i
	end
end

# ハッシュのキーを再帰的に全てシンボルに変換
def key_to_hash hash
	hash.inject({}) do |r,entry|
		if entry[1].instance_of? Hash
			entry[1] = key_to_hash(entry[1])
		end
		r.store( entry[0].to_sym, entry[1])
		r
  end
end

module Window
	def self.fillRect(x1, y1, w, h, c, option={})
		return if w == 0 || h == 0 
		x2, y2 = x1+w, y1+h
		x, y = [ x1, x2 ], [ y1, y2 ]
	  @@fill_img ||= Hash.new
	  @@fill_img[[w,h,c]] ||= Image.new(Math.abs(w),Math.abs(h),c)
		drawEx(x.min, y.min, @@fill_img[[w,h,c]], option)
	end
  def self.fillRectC(x1, y1, w, h, c, option={})
    self.fillRect(x1-w/2,y1-h/2,w,h,c,option)
  end
 def self.drawLine(x1, y1, x2, y2, c, z = 0)
    pixel = Image.new(1, 1, c)
    sx = Math.sqrt(((x1 - x2)**2) + ((y1 - y2)**2))
    angle = Math.atan2(y2 - y1, x2 - x1) / Math::PI * 180
    Window.drawEx((x2 + x1) / 2, (y2 + y1) / 2, pixel,
                  :scalex=>sx, :scaley=>1,
                  :centerx=>0.5, :centery=>0.5,
                  :angle=>angle, :z=>z)
  end
end
class RenderTarget
	def fillRect(x1, y1, w, h, c, option={})
		return if w == 0 || h == 0 
		x2, y2 = x1+w, y1+h
		x, y = [ x1, x2 ], [ y1, y2 ]
	  @@fill_img ||= Hash.new
	  @@fill_img[[w,h,c]] ||= Image.new(Math.abs(w),Math.abs(h),c)
		drawEx(x.min, y.min, @@fill_img[[w,h,c]], option)
	end
  def fillRectC(x1, y1, w, h, c, option={})
    fillRect(x1-w/2,y1-h/2,w,h,c,option)
  end
  def drawLine(x1, y1, x2, y2, c, z = 0)
    pixel = Image.new(1, 1, c)
    sx = Math.sqrt(((x1 - x2)**2) + ((y1 - y2)**2))
    angle = Math.atan2(y2 - y1, x2 - x1) / Math::PI * 180
    Window.drawEx((x2 + x1) / 2, (y2 + y1) / 2, pixel,
                  :scalex=>sx, :scaley=>1,
                  :centerx=>0.5, :centery=>0.5,
                  :angle=>angle, :z=>z)
  end
end
