# Imageクラスを拡張して縁取り/影付き文字描画ができるようにします。
# 引数はcolor1が文字の色、color2が縁取り/影の色です。
require 'dxruby'

module FramingFont
  def drawFramingFont(x, y, str, font, color1, color2)
    self.drawFont(x-1, y, str, font, :color => color2)
    self.drawFont(x+1, y, str, font, :color => color2)
    self.drawFont(x, y-1, str, font, :color => color2)
    self.drawFont(x, y+1, str, font, :color => color2)
    self.drawFont(x, y, str, font, :color => color1)
  end

  def drawShadowFont(x, y, str, font, color1, color2)
    self.drawFont(x+font.size/24+1, y+font.size/24+1, str, font, :color => color2)
    self.drawFont(x, y, str, font, :color => color1)
  end
end
class RenderTarget
	include FramingFont
end
class Image
	include FramingFont
end
module Window
  def self.drawFramingFont(x, y, str, font, color1, color2)
    drawFont(x-1, y, str, font, :color => color2)
    drawFont(x+1, y, str, font, :color => color2)
    drawFont(x, y-1, str, font, :color => color2)
    drawFont(x, y+1, str, font, :color => color2)
    drawFont(x, y, str, font, :color => color1)
  end

  def self.drawShadowFont(x, y, str, font, color1, color2)
    drawFont(x+font.size/24+1, y+font.size/24+1, str, font, :color => color2)
    drawFont(x, y, str, font, :color => color1)
  end
end
