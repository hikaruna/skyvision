# game_schene に include する
module CommandRender
  def get_msg player, card, cmd_name, multi=false
    card+=" など #{multi} 枚" if multi
    case cmd_name.to_sym
    when :play
      "#{player}: #{card} をプレイ"
    when :advent
      "#{player}: #{card} を場に出す"
    when :battle
      "#{player}: #{card} で戦闘"
    when :draw
      "#{player}: #{card} をドロー"
    when :graze
      "#{player}: グレイズ"
    when :manacharge
      "#{player}: マナチャージ"
    when :trush
      "#{player}: #{card} を破棄"
    when :exclude
      "#{player}: #{card} を除外"
    when :nodeset, :snode
      "#{player}: #{card} をノードにセット"
    when :public
      "#{player}: #{card} を公開"
    when :reverse
      "#{player}: #{card} を裏向きに"
    when :check
      "#{player}: #{card} を確認"
    when :shuffle
      "#{player}: #{card} をシャッフル"
    when :list
      "#{player}: #{card} を整列"
    when :add
      "#{player}: デッキを追加"
    when :move
      "#{player}: #{card} を移動"
    when :control
      "#{player}: #{card} の位置なおし"
    else
      "#{player}: #{card} を #{cmd_name}"
    end
  end
  def render_commands
    cl = 20 - @table.redo_stack.size
    cl = 10 if cl < 10
    rl = @table.redo_stack.size
    rl = 10 if rl > 10
    cmds = (@table.commands.last(cl)+@table.redo_stack.first(rl))
    msg = cmds.each_with_index do |cmd, i|
      if cmd.player_id
        player = @table.players[cmd.player_id]
        if player[:menber]
          player = player[:menber][:name]
        else
          player = "#{cmd.player_id+1}P"
        end
      else
        player = "ゲスト"
      end
      case cmd.name.to_sym
      when :firstset
        msg = "#{player}: デッキをセット"
      when :life
        life = @table.players[cmd.player_id][:life]
        type = cmd.order < 0 ? "ダメージ" : "回復"
        msg = "#{player}: #{Math.abs(cmd.order)} ポイントの #{type} 残 #{cmd.nokori}"
      when :message
        msg = "#{player}: #{cmd.order} を宣言"
      else
        if cmd.kind_of? UpdateCommand
          if cmd.order.size > 1
            card = @table.cards[cmd.order.first[:id]]
            card = card ? card.info(@player_id) : "????"
            msg = get_msg(player, card, cmd.name, cmd.order.size)
          else
            msg = cmd.order.map do |task|
              card = @table.cards[task[:id]]
              card = card ? card.info(@player_id) : "????"
              get_msg(player, card, cmd.name)
            end.join("\n")
          end
        else
          msg = get_msg(player, card, cmd.name)
        end
      end
      if !@table.redo_stack.empty? && cmd == @table.commands.last
        Window.fillRect(
          $conf[:commandinfo][:x], $conf[:commandinfo][:y]+i*10,
          Font.get(10).getWidth(msg), 10, [90,255,255,255])
      end
      Window.drawFont(
        $conf[:commandinfo][:x],
        $conf[:commandinfo][:y]+i*10, msg, Font.get(10))
    end

  end
end
