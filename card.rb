# カード一枚を表す
class Card
  @@library = EditAccessControl.new($conf[:cardlist_path]).GetConfigHash
  @@width, @@height = $conf[:card_icon_width], $conf[:card_icon_height]
  @@col_angle_pos =[
    [ 0, 0, @@width, @@height ],
    [ (@@width-@@height)/2, (@@height-@@width)/2, (@@width-@@height)/2+@@height, (@@height-@@width)/2+@@width ]]*2
    #[ 0, 0, @@width, @@height ],
    #[ -@@height, 0, @@height, @@width ],
    #[ -@@width, -@@height, @@width, @@height ],
    #[ 0, -@@width, @@height, @@width ] ]

  @@res = Hash.new
  def self.library
    @@library
  end
  def self.data
    @@library['Card']
  end
  def self.nums
    @@nums ||= @@library.keys - ['Game','Card']
  end

  def initialize no=nil
    @no = no
    @data = @@library[no]
    @state = 0 #:reverse, :public, :belongs
    @whose = 0
    @angle = 0
    @x = 0
    @y = 0
    @w, @h = $conf[:card_icon_width], $conf[:card_icon_height]
    @img = img
  end
  attr_accessor :info, :state, :whose, :angle, :x, :y,:w,:h, :data, :no
  def x
    @x.to_i
  end
  def y
    @y.to_i
  end
  def x1
    self.x - @w/2
  end
  def y1
    self.y - @h/2
  end
  def x2
    self.x + @w/2
  end
  def y2
    self.y + @h/2
  end

  def [] key
    d = @data[key.to_s.capitalize]
    d ? d.gsub('\n',"\n").chomp : ''
  end
  def active?(player_id=nil)
    player_id ||= 0
    player_id/2 == @angle%2
  end
  def sleep?(player_id=nil)
    !self.active?(player_id)
  end
  def img
    #unless @@res[@no]
      #path = $conf[:resource]+'cardillust/'+@no+'.png'
      #base = Image.new(@@width, @@height, [127,127,127])
      #if File.exist?(path)
        #illust = Image.load(path)
        #@@res[@no] = base.drawScale(
          #0,0,@@width,@@height,illust, 0,0,illust.width, illust.height)
      #else
        #@@res[@no] = base.drawScale(0,0,@@width,@@height,
          #Image.load($conf[:resource]+"reverse.png"),0,0,262,370)
      #end
    #end
    #@@res[@no]
    unless @@res[@no]
      img = Image.new(@@width, @@height, [127,127,196])
      img.drawFont(0,0,@no,Font.get(10))
      @data['Name'].split(/(.{4})/).grep(/.{1,}/).each_with_index do |s,i|
        img.drawFont(0,10+i*7, s, Font.get(7))
      end
    end
    @@res[@no] ||= img
  end
  def readble? player
    @state == :public || (@state == :belongs && player == @whose)
  end

  def rotPos
    case @angle
    when 0,2
      [self.x1,self.y1,@w,@h]
    when 1,3
      [self.x-@h/2,self.y-@w/2,@h,@w]
    end
  end
  def rotDPos
    @@col_angle_pos[@angle]
  end

  def info player
    if readble? player
      @data['Name'].chomp
    else
      "????"
    end
  end
  def id table
    id = table.cards.index(self)
    raise "id nil" if id.nil?
    id
  end
  def layer table
    l = table.layer_cards.index(self)
    raise "layer nil" if l.nil?
    l
  end
  def to_s
    @data.to_s
  end
  def to_task table
    { 
      :id => self.id(table),
      :x => self.x,
      :y => self.y,
      :state => self.state,
      :angle => self.angle,
      :whose => self.whose,
      :layer => self.layer(table),
    }
  end
  def inspect
    "<Card: #{@data['Name'].inspect}>"
  end
end
