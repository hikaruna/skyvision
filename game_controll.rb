module GameControll
  def save_replay filename
    puts "save to #{filename}"
    Dir::chdir("..") if Dir::getwd =~ Regexp.new($conf[:replay].sub('/','')+'$')

    File.open(filename,"w") do |f|
      YAML.dump(@table.commands.map{|cmd|cmd.to_hash}, f)
    end
  end
  def key_update
    @angle+=1 if Input.keyDown?(K_Q) && Input.keyDown?(K_LCONTROL)
    @angle += 90 if Input.keyPush?(K_T) && Input.keyDown?(K_LCONTROL)

    if Input.keyPush? K_ESCAPE
      if @player_id.nil?
        now = Time.now.strftime("%Y%m%d%H%m")
        #players = @table.players.map{|p|p[:member]}.compact.map{|p|"#{p[:name]}_#{p[:deck_name]}"}
        players = @table.members.map{|p|"#{p[:name]}"}
        save_replay($conf[:replay]+"#{now}_#{players.join("_vs_")}.rep")
        @next_scene = Scene::Exit.new if Input.keyPush? K_ESCAPE
      else
        @master.leave @player_id
        @player_id = nil
      end
    end

    # メッセージ
    if kc=(Input.keyDown?(K_LSHIFT)||Input.keyDown?(K_RSHIFT)) ? :shift : :free
      $conf[:message_code][kc].each do |k,v|
        if Input.keyPush?(k)
          @master.exec(
            MessageCommand.new(
              v, @player_id, @selected.map{|c|c.id(@table)}).dump)
        end
      end

      if Input.keyDown?(K_LCONTROL) || Input.keyDown?(K_RCONTROL) 
        if Input.keyPush?(K_Z)
          @master.undo if @player_id
        end
        if Input.keyPush?(K_Y)
          @master.redo if @player_id
        end
        if Input.keyPush?(K_R)
          deck_set
        end

        if Input.keyPush? K_1
          @master.leave 0
        end
        if Input.keyPush? K_2
          @master.leave 1
        end
        if Input.keyPush? K_3
          @master.leave 2
        end
        if Input.keyPush? K_4
          @master.leave 3
        end
        if Input.keyPush? K_S
          filename = Window.saveFilename( [["リプレイファイル(*.rep)", "*.rep"]], "リプレイ保存" )
          save_replay(filename) if filename
        end
        if Input.keyPush? K_O
          filename = Window.openFilename( [["リプレイファイル(*.rep)", "*.rep"]], "リプレイ読込" )
          if filename
            cmds = YAML.load_file(filename)
            cmds.map{|cmd|Marshal.dump(cmd)}.each do |cmd|
              @master.exec(cmd)
            end
          end
        end
      elsif Input.keyDown?(K_LSHIFT) || Input.keyDown?(K_RSHIFT)
      else
        if Input.keyPush? K_K
          if @player_id.nil?
            @player_id = @master.join( {
              :name => $conf[:profile][:name],
              :id   => @id,
              :deck_name => @deck[:name],
              :life => 25,
            })
            p @player_id
            @angle = [0,180,90,270 ][@player_id]
            unless @table.cards.find{|c|c.whose == @player_id}
              @master.exec(AddCommand.new(@deck[:list], @player_id).dump)
            end
          end
        end
        if Input.keyPush? K_O
          task = @selected.map {|c| c.to_task(@table) }
          cmd = UpdateCommand.new(task, @player_id, :sort)
          cmd.layer_sort.layer_sort
          @master.exec(cmd.dump)
        end
        if Input.keyPush? K_J
          if @player_id.nil?
            @player_id = @master.join( {
              :name => $conf[:profile][:name],
              :id   => @id,
              :deck_name => @deck[:name],
              :life => 25,
              :url => $conf[:url]
            })
            if @player_id
              @angle = [0,180,90,270 ][@player_id]
              unless @table.cards.find{|c|c.whose == @player_id}
                @master.exec(AddCommand.new(@deck[:list], @player_id).dump)
                deck_set
              end
            end
          end
        end

        if Input.keyPush?(K_DOWN)
          @life_diff -= 1
        elsif Input.keyPush?(K_UP)
          @life_diff += 1
        elsif Input.keyPush?(K_RETURN) && @life_diff != 0
          @master.exec(LifeCommand.new(@life_diff, @player_id).dump)
          @life_diff = 0
        end

        if Input.keyPush? K_E
          cmd = UpdateCommand.new(
            @selected.map{|c| {:id=>@table.cards.index(c)}},
            @player_id, :battle )
            cmd.sleep_order @table
            @master.exec(cmd.dump)
        elsif Input.keyPush? K_Y
          @master.exec(MessageCommand.new("先攻後攻決定権は#{rand(2)==0 ? "もらった" : "あげよう"}", @player_id).dump )
        end

        if Input.keyPush?(K_G) || Input.keyPush?(K_M)
          x =  $conf[:zone][:deck][:x]
          y =  $conf[:zone][:deck][:y]
          w =  $conf[:zone][:deck][:w]
          h =  $conf[:zone][:deck][:h]
          deck = card_select x, y, w, h
          puts "deck => #{deck.inspect}"
          puts "deck.last => #{deck.last.inspect}"
          target = deck.last
          if target
            mx, my = turn($conf[:zone][:snode][:x]-35,$conf[:zone][:snode][:y]-30,true)
            while @table.cards.find{|c| c.x > mx-1 && c.x < mx+1 && c.y >my-1 && c.y < my+1 }
              v = Vector.new(1,3,1,1) * Matrix.createRotationZ(@angle)
              mx+=v.x
              my+=v.y
            end
            task = [ {
              :id => target.id(@table),
              :x => mx,
              :y => my,
              :layer => :top,
            }]
            code = Input.keyPush?(K_G) ? :graze : :manacharge
            cmd = UpdateCommand.new(task, @player_id, code).snode(@table)
            @master.exec(cmd.dump)
          end
        end

        if !@selected.empty?
          if Input.keyPush? K_S
            task = @selected.map{|c|{ :id=>@table.cards.index(c) }}
            cmd = UpdateCommand.new(task, @player_id, :sleep ).sleep_order(@table)
            @master.exec(cmd.dump)
          end
          if Input.keyPush? K_A
            task = @selected.map{|c| {:id => @table.cards.index(c)} }
            cmd = UpdateCommand.new(task, @player_id, :active ).active(@table)
            @master.exec(cmd.dump)
          end
          if Input.keyPush? K_D
            task = @selected.map{|c| {
              :id => @table.cards.index(c),
              :angle => (c.angle+1)%4,
            } }
            @master.exec(UpdateCommand.new(task, @player_id).dump)
          end
          if Input.keyPush? K_Z
            order =   @selected.map{|c| @table.cards.index(c) }
            if @selected.find{|c| c.state != :public }
              task = @selected.map do |c|
                { :id => @table.cards.index(c), :state => :public }
              end
              @master.exec(UpdateCommand.new(task, @player_id, :public ).dump )
            end
          end
          if Input.keyPush? K_X
            order =   @selected.map{|c| @table.cards.index(c) }
            if @selected.find{|c| c.state != :reverse }
              task = @selected.map do |c|
                { :id => @table.cards.index(c), :state => :reverse }
              end
              @master.exec(UpdateCommand.new(task, @player_id, :reverse ).dump )
            end
          end
          if Input.keyPush? K_C
            order =   @selected.map{|c| @table.cards.index(c) }
            if @selected.find{|c| c.state != :belongs }
              task = @selected.map do |c|
                { :id => @table.cards.index(c), :state => :belongs }
              end
              @master.exec(UpdateCommand.new(task, @player_id, :check ).dump )
            end
          end
          if Input.keyPush? K_V
            if @detail
              i = @selected.index(@detail)
              @detail = @selected[i+1] if i && i+1 < @selected.size
            end
          end
          if Input.keyPush? K_B
            if @detail
              i = @selected.index(@detail)
              @detail = @selected[i-1] if i && i-1 >= 0
            end
          end
          if Input.keyPush? K_L
            f = @selected.first
            mx, my = turn(Input.mousePosX, Input.mousePosY, true)
            tasks = []
            @selected.each_with_index do |c,i|
              tasks << {
                :id => @table.cards.index(c),
                :x  => f.x + (mx-f.x)*i/@selected.size,
                :y  => f.y + (my-f.y)*i/@selected.size,
              }
            end
            @master.exec UpdateCommand.new(tasks, @player_id, :list).dump
          end
          if Input.keyPush?(K_SPACE)
            mx, my = turn(Input.mousePosX, Input.mousePosY, true)
            while @table.cards.find{|c| c.x > mx-1 && c.x < mx+1 && c.y >my-1 && c.y < my+1 }
              v = Vector.new(3,3,1,1) * Matrix.createRotationZ(@angle)
              mx+=v.x
              my+=v.y
            end
            c = @selected.shift
            task = [ {
              :id => c.id(@table),
              :x => mx,
              :y => my,
              :layer => :top,
            }]
            from = @zone.find{|z|z.x1<c.x1&&c.x2<z.x2&&z.y1<c.y1&&c.y2<z.y2}
            to = @zone.find{|z|z.x1<mx && mx<z.x2 && z.y1<my && my<z.y2}
            from = from.name if from
            to = to.name if to
            cmd = move(UpdateCommand.new(task,@player_id), from, to)
            @master.exec(cmd.dump)
          end

          if Input.keyPush? K_H
            task = @selected.map {|c| c.to_task @table }
            cmd = UpdateCommand.new(task, @player_id, :shuffle).shuffle @table
            @master.exec(cmd.dump)
            self.selected = @selected.sort_by{|c|c.layer(@table)}
          end

        end
      end
    end
  end
  def mouse_update
    # マウスのボタンを押したとき
    if Input.mousePush?(M_LBUTTON)
      @mx, @my = Input.mousePosX, Input.mousePosY
      tmp = card_select(@mx,@my)

      # すでに選択済みカードをクリックしたとき
      unless (@selected & tmp).empty?
        @draging = true
        self.selected = @selected
      else
        self.selected = tmp if tmp.first == @detail
        # 何も無いところでクリックしたとき
        if @selected.empty?
          @box = [ @mx, @my ]
        else
          @draging = true
        end
      end

      # マウスのボタンを放したとき
    elsif !Input.mouseDown?(M_LBUTTON)
      mx2, my2 = Input.mousePosX, Input.mousePosY
      if @mx == mx2 && @my == my2
        if @detail != @selected.first
          @draging = true
          self.selected = @detail ? [@detail] : []
        end
        @box = nil
        @draging = false
      else
        # カードをドラッグ中のとき
        if !@selected.empty? && @draging
          task = []
          sorted = @selected.sort{|a,b|
            @table.layer_cards.index(a) <=> @table.layer_cards.index(b)
          }.each_with_index do |c,i|
            diff = @selected_diff.reverse[i]
            x, y = turn(
              mx2+diff[0], my2+diff[1], true )
              task << {
                :id => @table.cards.index(c),
                :x  => x, 
                :y  => y,
                :layer => @table.cards.size-1
              }
          end
          from = @zone.find do |z|
            z.x1 < @mx && @mx <z.x2 && z.y1 < @my && @my < z.y2
          end
          to = @zone.find do |z|
            z.x1 < mx2 && mx2 <z.x2 && z.y1 < my2 && my2 < z.y2
          end
          from = from.name if from
          to = to.name if to
          cmd = move(UpdateCommand.new(task,@player_id),from, to)
          begin
            @master.exec(cmd.dump)
          rescue=>e
            p cmd
            raise e
          end

          @draging = false

          # 範囲選択中のとき
        elsif @box
          self.selected = card_select(@mx, @my, mx2-@mx, my2-@my)
          @box = nil
        end
      end
    end
  end

  def card_select x, y, w=0, h=0
    select = []
    @table.layer_cards.reverse.each do |card|
      cx, cy, cw, ch = card.rotPos
      cx1, cy1 = turn(cx, cy)
      cx2, cy2 = turn(cx+cw, cy+ch)
      cx = [cx1,cx2]
      cy = [cy1,cy2]
      mx ||= [ x, x+w ]
      my ||= [ y, y+h ]
      if(cx.min < mx.max && mx.min < cx.max &&
         cy.min < my.max && my.min < cy.max  )
        select << card
        #tmx, tmy = turn(@mx,@my,true)
        break if w==0 && h==0 # 単数選択の時は最初の一枚で終了
      end
    end
    return select
  end

  def move(cmd, from, to)
    if from == :deck && to == :hand
      cmd.draw(@table).name = :draw
    elsif from == :hand && to == :anode
      cmd.nodeset(@table).name = :nodeset
    elsif from == :hand && to == :play
      cmd.play(@table).name = :play
    elsif from != :field && to == :field
      cmd.name = :advent
      if from == :play && to == :field && cmd.order.size == 1
        card = @table.cards[cmd.order.first[:id]]
        if (card['Type'] == 'Character' || (card['Skill'].match(/幻想生物/)) ) && card['Skill'].match(/速攻/).nil?
          cmd.sleep_order(@table)
        end
      end
      cmd.bottom(@table)
    elsif from == :deck && to == :snode
      cmd.snode(@table).name = :snode
    elsif from == :trush && to == :hand
      c = @table.cards[cmd.order.first[:id]]
      @master.exec(MessageCommand.new("#{c[:name]}を回収", @player_id).dump)
      cmd.draw(@table).name = :recovery
    elsif from != :play && to == :play
      cmd.public_order.name = :public
    elsif from != :trush && to == :trush
      cmd.trush(@table).name = :trush
    elsif from != :exclude && to == :exclude
      cmd.exclude(@table).name = :exclude
    elsif from != :hand && to == :hand
      cmd.draw(@table)
    elsif from != :snode && to == :snode
      cmd.snode(@table).name = :snode
    elsif from == to
      cmd.name = :control
    else
      cmd.name = :move
    end
    Input.keyDown?(K_SPACE)||cmd.bottom? ? cmd.bottom : cmd.top(@table)
  end
  def deck_set
    yamahuda_pos = Array.new(43) do |i|
      [ $conf[:zone][:deck][:x]+i/7,
        $conf[:zone][:deck][:y]+i/5, ]
    end
    tehuda_pos = Array.new(7) do |i|
      h  = $conf[:zone][:hand]
      cw = $conf[:card_icon_width]
      [ h[:x] - cw/2*6 + (cw+3)*i,
        h[:y] +i/5, ]
    end
    pos = (yamahuda_pos + tehuda_pos).map{|i|
      dx = @render_target.width/2
      dy = @render_target.height/2
      cc = Vector.new(
        $conf[:card_icon_width]/2,$conf[:card_icon_height]/2,0,1)
        angle = 90*[0,2,3,1][@player_id]
        v = Vector.new(i[0],i[1],1,1)*
          Matrix.createTranslation(-dx,-dy,0)*
          Matrix.createRotationZ(angle)*
          Matrix.createTranslation(dx,dy,0)
        { :x => v.x, :y => v.y }
    }
    state = Array.new(43, :reverse) + Array.new(7, :belongs)
    id = @table.cards.select{|c| c.whose == @player_id }.map{|c| @table.cards.index(c)}
    layer = id.map{|i| @table.layer_cards.index(@table.cards[i])}
    raise if pos.size != 50
    raise if state.size != 50
    raise "デッキカード枚数が#{id.size}です" if id.size != 50
    task = Array.new(50) do |i|
      { :id => id[i],
        :state => state[i],
        :x => pos[i][:x],
        :y => pos[i][:y],
        :layer => layer[i],
      }
    end
    cmd = UpdateCommand.new(task, @player_id, :firstset).shuffle(@table).layer_sort
    @master.exec(cmd.dump)
  end
end
