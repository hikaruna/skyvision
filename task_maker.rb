# UpadateCommand ��include����
module TaskMaker
  def public_order
    @order.each{|task| task[:state] = :public }
    self
  end
  def reverse
    @order.each{|task| task[:state] = :reverse }
    self
  end
  def check
    @order.each{|task| task[:state] = :belongs }
    self
  end
  def play table
    active table
    public_order
  end
  def draw table
    active table
    check
  end
  def nodeset table
    active table
    reverse
  end
  def snode table
    sleep_order table
    reverse
  end
  def trush table
    active table
    public_order
  end
  def exclude table
    sleep_order table
    public_order
  end
  def active table
    @order.each do |task|
      ori_angle = table.cards[task[:id]].angle
      a = [ 0,0,2,2 ]
      task[:angle] = [
        a.values_at(0,1,2,3)[ori_angle],
        a.values_at(0,1,2,3)[ori_angle],
        [3,1,1,3][ori_angle],
        [3,1,1,3][ori_angle],
      ][@player_id]
    end
    self
  end
  def sleep_order table
    @order.each do |task|
      c = table.cards[task[:id]]
      ori_angle = table.cards[task[:id]].angle
      a = [ 1,1,3,3 ]
      task[:angle] = [
        a.values_at(0,1,2,3)[ori_angle],
        a.values_at(0,1,2,3)[ori_angle],
        [0,2,2,0][ori_angle], #a.values_at(1,2,3,0)[ori_angle],
        [0,2,2,0][ori_angle], #a.values_at(3,0,1,2)[ori_angle],
      ][@player_id]
    end
    self
  end
  def shuffle table
    @order.each {|task| task[:layer]= table.layer_cards.index(table.cards[task[:id]])}
    puts "error 003 order->#{@order.inspect[0...100]}"
    id = @order.map {|task| task[:id]}
    #layer = @order.map {|task| table.layer_cards.index(table.cards[task[:id]])}
    puts "in shuffle. shuffle start order->#{@order.sort_by{|t|t[:id]}.inspect}"
    @order.shuffle!
    #puts "error 002 order->#{@order.inspect[0...100]}"
    @order.each_with_index do |task,i|
      task[:id]=id[i]
      #task[:layer]=layer[i]
    end
    puts "in shuffle. shuffle done order->#{@order.sort_by{|t|t[:id]}.inspect}"
    self
  end
  def top table
    @order.each_with_index do |task,i|
      task[:layer] = table.layer_cards.size-1-i
    end
    self
  end
  def bottom table=nil
    puts "called bottom !"
    @order.each_with_index do |task,i|
      task[:layer] = @order.size-i
    end
    @bottom = true
    self
  end
  def bottom?
    @bottom
  end
  def layer_sort
    l = @order.map{|t| t[:layer]}.sort.reverse
    5.times do
    @order.sort_by{|t| t[:x]}.each_with_index do |task,i|
      task[:layer] = l[i]
    end
    end
    self
  end
end
