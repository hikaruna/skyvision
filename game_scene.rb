class GameScene < Scene::Base
  require 'command_render'
  include GameControll, CommandRender

  def init option
    puts "GameScene init option=> #{option.inspect}"
    @master = option[:master]
    @table = option[:table]
    @id = option[:id]
    @deck = option[:deck]
    @player_id = nil
    @mouse_downed = false
    $conf[:table_width] ||= 450
    $conf[:table_height] ||= 450
    @render_target = RenderTarget.new(
      $conf[:table_width],$conf[:table_height])
    @render_target.bgcolor = [32,140,64]
    @angle =0
    @selected = []
    @selected_diff = []
    @mx, @my = 0, 0
    @zone = $conf[:zone].keys.map{|name| Zone.new name}
    @life_diff = 0
    @msgcount = 0

    @table_x, @table_y, @table_angle = 0, 0, 0
    @areaselect = false
  end
  def update

    key_update
    mouse_update

    #if (@selected && !(@selected.size > 1)) || !@detail
    @oldm ||= { :x => Input.mousePosX, :y => Input.mousePosY }
    if @oldm[:x] != Input.mousePosX || @oldm[:y] != Input.mousePosY
      @detail = card_select(Input.mousePosX,Input.mousePosY).first
    end
    @oldm = { :x => Input.mousePosX, :y => Input.mousePosY }

  rescue=>e
    now = Time.now.strftime("%Y%m%d%H%m")
    players = @table.members.map{|p|"#{p[:name]}"}
    save_replay($conf[:replay]+"#{now}_#{players.join("_vs_")}.rep")
    puts "現在の状態を保存しました"
    raise e
  end

  def render
    # カードの描画
    @table.layer_cards.each do |card|
      # 表のカードを描画
      if card.readble? @player_id
          res = CardImg[card.no]
          puts "no->#{card.no.inspect}" if !res
          exit if !res
        @render_target.drawRot(
          card.x1, card.y1, CardImg[card.no], card.angle*90)


        # 自分だけが見えているカードを赤くする
        if card.state == :belongs
          x, y, w, h = card.rotPos
          v =  $sin[@frame_counter*3%360]
          @render_target.fillRect( card.x1, card.y1, card.w, card.h,
            [96+40*v,255,40+v*30, 40+v*30], :angle => card.angle*90 )
        end

      # 裏のカードを描画
      else
        @render_target.drawRot(
          card.x1, card.y1, CardImg.reverse(card.whose), card.angle*90)
      end
    end

    # カードをドラッグ中に移動先カーソル描画
    if @draging
      v = @frame_counter*15%255
      @selected.each_with_index do |c,i|
        diff = @selected_diff[i]
        mx1, my1 = turn(Input.mousePosX+diff[0], Input.mousePosY+diff[1], true)
        @render_target.fillRectC(mx1, my1, c.w, c.h, [v,v,v,v], :angle => c.angle*90)
      end
    end

    # 選択中カードを青くする
    @selected.each do |c|
      @render_target.fillRectC( c.x, c.y, c.w, c.h,
        [0,96,160], :angle => c.angle*90, :alpha => 40)
    end

    Window.drawFont(0,0,Window.fps.to_s, Font.get(30))
    @mouse_downed = Input.mouseDown?(M_LBUTTON)
    @render_target.update


    Window.drawFont(0,10,@angle.to_s,Font.get(32))
    # 参加中
    if @player_id
      Window.drawRot(@table_x,@table_y, @render_target, @angle,
        @render_target.width/2, @render_target.height/2 )
    #観戦中
    else
      Window.draw(@table_x,@table_y, @render_target)
    end

    # 範囲選択中に範囲を描画
    if @box
      Window.fillRect(@box[0], @box[1],
        Input.mousePosX-@box[0], Input.mousePosY-@box[1],[128,255,255,255])
    end

    if !@player_id
      Window.drawFont(500, 50, "参加する？", Font.get(20))
      Window.drawFont(500, 70, "push J", Font.get(20)) if @frame_counter%100 < 50
    end

    # ダメージ
    if @life_diff != 0
      msg = @life_diff > 0 ? "+#{@life_diff}" : @life_diff.to_s
      Window.drawFont(
        @render_target.width/2-80,
        @render_target.height/2-32,
        "#{msg}\nEnter で確定", Font.get(32))

    end
    # 観戦者情報表示
    Window.drawFont($conf[:members_info][:x],$conf[:members_info][:y],
      (["メンバー"]+@table.members.map{|m|m[:name]}).join("\n"), Font.get(10) )

    # 参加者情報表示
    @member_info ||= RenderTarget.new(300,350)
    @member_info.drawFont(0,0, "参加者", Font.get(10))
    @table.players.map{|p|p[:menber]}.each_with_index do |player,i|
      @member_info.drawFont(0,15+i*10, player[:name], Font.get(10)) if player
    end
    @table.players.each_with_index do |player,i|
      @member_info.drawFont(80,15+i*10, player[:life].to_s, Font.get(10)) if player[:menber]
    end
    @member_info.update
    Window.draw($conf[:players_info][:x], $conf[:players_info][:y],@member_info)

    render_commands

    Window.drawFont(300,0,"#{@selected.size}", Font.get(48))

    if @selected.size > 1
      @selected.each_with_index do |c,i|
        Window.drawFont(
          $conf[:selected_list][:x],
          $conf[:selected_list][:y]-12,
          "#{@selected.size} 枚", Font.get(12) )
        Window.drawFont(
          $conf[:selected_list][:x],
          $conf[:selected_list][:y]+i*8,
          c.info(@player_id), Font.get(8) )
        if c == @detail
          Window.fillRect(
            $conf[:selected_list][:x], $conf[:selected_list][:y]+i*8, 
            Font.get(8).getWidth(c.info(@player_id)), 8,
            [255,255,0], :alpha => 180 )
        end
      end
    end
      
    # カード詳細表示
    @detail_info ||= RenderTarget.new(180, 270)
    if @detail
      if @detail.readble?(@player_id)
        res = CardImg.get_bigimg @detail.no
        @detail_info.drawScale( 0, 0, res,
          @detail_info.width/res.width.to_f,
          @detail_info.height/res.height.to_f, 0,0)
        @detail_info.fillRect( 0, 0,
          @detail_info.width, @detail_info.height, [0,0,0], :alpha => 20)
        cardinfo = <<-EOS
#{@detail.no} #{@detail[:type]}
#{@detail[:name]}
G #{@detail[:glaze]}#{" "*30}N #{@detail[:node]}/C #{@detail[:cost]}
#{@detail[:udetail].empty? ? '' : "Udetail #{@detail[:udetail]}"}#{@detail[:upkeep].empty? ? '' : ", 維持 #{@detail[:upkeep]}"}
#{@detail[:class]}
#{@detail[:skill]}
#{@detail[:ability].scan(/.{1,18}/).join("\n")}\n
#{@detail[:text].scan(/.{1,18}/).join("\n  ")}

#{@detail[:attack]} / #{@detail[:toughness]}
        EOS
        if !Input.keyDown?(K_LSHIFT)&&!Input.keyDown?(K_RSHIFT)
          @detail_info.drawFramingFont(
            10, 10, cardinfo, Font.get(10), [255,255,255], [0,0,0])
        end
      end
    end
    @detail_info.update
    Window.draw(
      $conf[:detail][:x],
      $conf[:detail][:y], @detail_info)

    @zone.each do |z|
      Window.fillRectC(z.x, z.y, z.w, z.h, z.c, :alpha=>48)
    end

    if Input.keyDown?(K_F1)
      @help_msg ||= $conf[:message][:shift].to_s
      Window.drawFramingFont(100,100,@help_msg,[0,0,0],[0,0,0])
    end

    if @table.msg
      if @msgcount > 0
        Window.fillRect(0,280,
          @render_target.width, 40,[32,32,64], :alpha=>@msgcount)
        Window.drawFont(300-Font.get(40).getWidth(@table.msg)/2,280,
          @table.msg, Font.get(40), :alpha=>@msgcount)
        @msgcount -= 3
      else
        @table.msg = nil
      end
    else
      @msgcount = 255
    end
    @msgcount = 255 unless @oldmsg.equal? @table.msg
    @oldmsg = @table.msg

  rescue=>e
    begin
      backup
      puts "現在の状態を保存しました"
    ensure
      raise e
    end
  end
  def turn(x, y, reverse=false)
    angle = reverse ? -@angle : @angle
    centerx, centery = @render_target.width/2, @render_target.height/2
    [(x-centerx)*$cos[angle%360] -
     (y-centery)*$sin[angle%360] + centerx,
     (x-centerx)*$sin[angle%360] +
     (y-centery)*$cos[angle%360] + centery ]
  end

  def selected= selected
    @selected = selected
    @selected_diff = selected.map do |c|
      cx, cy = turn(c.x, c.y)
      [ cx-@mx, cy-@my ]
    end
  end
end
