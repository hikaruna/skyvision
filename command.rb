class Command
  attr_accessor :name, :order
  def initialize order, player_id=nil, name=nil
    @name = name
    @name ||= self.class.name.sub(/Command$/,'').downcase
    @order = order
    @player_id = player_id
  end
  def to_hash
    { :type => self.class.name,
      :name => @name,
      :order => @order,
      :player_id => @player_id, }
  end
  def dump
    puts "called Command.dump"
    s = self.to_hash
    puts "self.dumpfrom is #{s.inspect}"
    str = Marshal.dump(s)
  rescue=>e
    puts "self is #{self.inspect[0...100]}"
    puts "self.dumpfrom is #{s.inspect}"
    puts "self.dump is #{str.inspect[0...100]}"
    raise e
  end
  def self.load_hash(hash)
    case hash[:type]
    when "AddCommand"
      AddCommand.new(hash[:order], hash[:player_id], hash[:name])
    when "UpdateCommand"
      UpdateCommand.new(hash[:order], hash[:player_id], hash[:name])
    when "LifeCommand"
      LifeCommand.new(hash[:order], hash[:player_id], hash[:name])
    when "MessageCommand"
      MessageCommand.new(hash[:order], hash[:player_id], hash[:target])
    end
  end
  def self.load(str)
    self.load_hash(Marshal.load(str))
  end
  attr_reader :name, :order, :player_id
  def exec table
    update table
  end
  def update table
  end
  def revert table
    true
  end
  def msg player_id=nil
    "#{@player_id}: #{@name}"
  end
end

class AddCommand < Command
  def update table
    # task = [ cardno, cardno, ]
    new_cards = @order.map do |no|
      card = Card.new(no)
      card.whose = @player_id
      card.angle = [0,2,3,1][@player_id]
      card
    end
    table.cards += new_cards
    table.layer_cards += new_cards
    @added_id = new_cards.map{|c| table.cards.index(c) }
  end
  def revert table
    @added_id.each { |id| table.cards[id] = nil }
    table.cards.compact!
    table.layer_cards &= table.cards
  end
end
class LifeCommand < Command
  attr_reader :nokori
  def update table
    table.players[@player_id][:life] += order
    @nokori = table.players[@player_id][:life]
  end
  def revert table
    table.players[@player_id][:life] -= order
  end
end
class MessageCommand < Command
  def initialize msg, player_id, target=nil
    @target = target
    super msg, player_id
  end
  def to_hash
    { :type => self.class.name,
      :order => @order,
      :player_id => @player_id,
      :target => @target, }
  end
  def update table
    return nil unless @player_id
    if @order=~/%s/
      if @target && !@target.empty?
        s = @target.size
        f = table.cards[@target.first].info(@player_id)
        target = s>1 ? "#{f} など #{s}枚" : f
        table.msg = @order.sub!('%s',target)
      else
        return nil
      end
    else
      table.msg = @order
    end
  end
end

class UpdateCommand < Command
  require 'task_maker'
  include TaskMaker
  def update table
    puts "called UpdateCommand#update() ! "
    return unless @player_id
    return unless @order

    puts "UpdateCmmand#update(table) is called !"
    puts "order =>#{@order.inspect}"
    puts "cards =>#{table.cards.values_at(*(@order.map{|t|t[:id]})).inspect}"

    # order の最適化
    return nil if @order.map do |task|
      card = table.cards[task[:id]]
      task = task.dup
      task.delete(:x) if task[:x] == card.x
      task.delete(:y) if task[:y] == card.y
      task.delete(:state) if task[:state] == card.state
      task.delete(:angle) if task[:angle] == card.angle
      task.delete(:whose) if task[:whose] == card.whose
      task.delete(:layer) if task[:layer] == table.layer_cards.index(card)
      task
    end.reject{|task| task.keys == [:id] }.empty?

    @froms = @order.map do |task|
      h = {}
      card = table.cards[task[:id]]
      h[:id] = task[:id]
      h[:x] = card.x if task.include?(:x)
      h[:y] = card.y if task.include?(:y)
      h[:state] = card.state if task.include?(:state)
      h[:angle] = card.angle if task.include?(:angle)
      h[:whose] = card.whose if task.include?(:whose)
      h[:layer] = table.layer_cards.index(card) if task.include?(:layer)
      h
    end

    replace @order, table
    return true
  end
  def replace order, table
    order.each do |task|
      card = table.cards[task[:id]]
      card.x = task[:x] if task.include?(:x)
      card.y = task[:y] if task.include?(:y)
      card.state = task[:state] if task.include?(:state)
      card.angle = task[:angle] if task.include?(:angle)
      card.whose = task[:whose] if task.include?(:whose)
    end
    lorder = order.select{|task| task.include?(:layer)}
    #back = lorder.map { |task| table.layer_cards[task[:layer]] }
    #cards= lorder.map { |task| table.cards[task[:id]] }
    #if (back - cards).empty?
      lorder.each do |task|
        table.layer_cards.insert(
          task[:layer], table.layer_cards.delete(
            table.cards[task[:id]] ) )
      end
      table.layer_cards.compact!
    #else
      #puts "in UpdateCommand#replace. error 005 !!!!!!!!
      #back->#{back.map{|c|c.to_task(table)}.inspect}
      #cards->#{cards.map{|c|c.to_task(table)}.inspect}"
      #puts "in UpdateCommand#replace. error 005 !!!!!!!!"
    #end
  end
  def revert table
    replace @froms.reverse, table
  end
end


#deck = ["112", "204", "251", "342", "342", "342", "353", "364", "370", "421" , "440", "512", "517", "529", "530", "533", "538", "079", "083", "084", "092", " 142", "247", "282", "397", "397", "467", "553", "557", "580", "580", "006", "015 ", "303", "303", "314", "327", "329", "333", "473", "475", "479", "479", "480", "482", "488", "492", "494", "495", "504"]
#update = [{:layer=>19, :x=>536.0, :y=>511.0, :state=>:reverse, :id=>0}, {:layer=>46, :x=> 281.25, :y=>525.0, :state=>:belongs, :id=>1}, {:layer=>21, :x=>535.0, :y=>510.0, :state=>:reverse, :id=>2}, {:layer=>45, :x=>325.0, :y=>525.0, :state=>:belongs, :id=>3}, {:layer=>29, :x=>531.0, :y=>506.0, :state=>:reverse, :id=>4}, {:layer=>5, :x=>543.0, :y=>518.0, :state=>:reverse, :id=>5}, {:layer=>31, :x=>530.0, :y=>505.0, :state=>:reverse, :id=>6}, {:layer=>39, :x=>526.0, :y=>501.0, :state=>:reverse, :id=>7}, {:layer=>25, :x=>533.0, :y=>508.0, :state=>:reverse, :id=>8}, { :layer=>7, :x=>542.0, :y=>517.0, :state=>:reverse, :id=>9}, {:layer=>47, :x=>237.5, :y=>525.0, :state=>:belongs, :id=>10}, {:layer=>8, :x=>542.0, :y=>517.0, :state=>:reverse, :id=>11}, {:layer=>23, :x=>534.0, :y=>509.0, :state=>:reverse, :id=>12}, {:layer=>27, :x=>532.0, :y=>507.0, :state=>:reverse, :id=>13}, {:layer=> 30, :x=>531.0, :y=>506.0, :state=>:reverse, :id=>14}, {:layer=>1, :x=>545.0, :y=>520.0, :state=>:reverse, :id=>15}, {:layer=>37, :x=>527.0, :y=>502.0, :state=>:reverse, :id=>16}, {:layer=>2, :x=>545.0, :y=>520.0, :state=>:reverse, :id=>17}, {:layer=>41, :x=>525.0, :y=>500.0, :state=>:reverse, :id=>18}, {:layer=>5, :x=> 543.0, :y=>518.0, :state=>:reverse, :id=>19}, {:layer=>43, :x=>412.5, :y=>525.0, :state=>:belongs, :id=>20}, {:layer=>11, :x=>540.0, :y=>515.0, :state=>:reverse, :id=>21}, {:layer=>44, :x=>368.75, :y=>525.0, :state=>:belongs, :id=>22}, {:layer=>0, :x=>546.0, :y=>521.0, :state=>:reverse, :id=>23}, {:layer=>9, :x=>541.0, :y=>516.0, :state=>:reverse, :id=>24}, {:layer=>23, :x=>534.0, :y=>509.0, :state=>:reverse, :id=>25}, {:layer=>31, :x=>530.0, :y=>505.0, :state=>:reverse, :id=>26}, {:layer=>35, :x=>528.0, :y=>503.0, :state=>:reverse, :id=>27}, {:layer=>26, :x=>533.0, :y=>508.0, :state=>:reverse, :id=>28}, {:layer=>15, :x=>538.0, :y=>513.0, :state=>:reverse, :id=>29}, {:layer=>48, :x=>193.75, :y=>525.0, :state=>:belongs, :id=>30}, {:layer=>3, :x=>544.0, :y=>519.0, :state=>:reverse, :id=>31}, {:layer=>49, :x=>150.0, :y=>525.0, :state=>:belongs, :id=>32}, {:layer=>13, :x=>539.0, :y=>514.0, :state=>:reverse, :id=>33}, {:layer=>33, :x=>529.0, :y=>504.0, :state=>:reverse, :id=>34}, {:layer=>11, :x=>540.0, :y=>515.0, :state=>:reverse, :id=>35}, {:layer=>19, :x=>536.0, :y=>511.0, :state=>:reverse, :id=>36}, {:layer=>38, :x=>527.0, :y=>502.0, :state=>:reverse, :id=>37}, {:layer=>13, :x=>539.0, :y=>514.0, :state=>:reverse, :id=>38}, {:layer=>22, :x=>535.0, :y=>510.0, :state=>:reverse, :id=>39}, {:layer=>28, :x=>532.0, :y=>507.0, :state=>:reverse, :id=>40}, {:layer=>16, :x=>538.0, :y=>513.0, :state=>:reverse, :id=>41}, {:layer=>39, :x=>526.0, :y=>501.0, :state=>:reverse, :id=>42}, {:layer=>17, :x=>537.0, :y=>512.0, :state=>:reverse, :id=>43}, {:layer=>10, :x=>541.0, :y=>516.0, :state=>:reverse, :id=>44}, {:layer=>18, :x=>537.0, :y=>512.0, :state=>:reverse, :id=>45}, {:layer=>4, :x=>544.0, :y=>519.0, :state=>:reverse, :id=>46}, {:layer=>34, :x=>529.0, :y=>504.0, :state=>:reverse, :id=>47}, {:layer=>35, :x=>528.0, :y=>503.0, :state=>:reverse, :id=>48}, {:layer=>42, :x=>525.0, :y=>500.0, :state=>:reverse, :id=>49}]
#p c=Marshal.dump(AddCommand.new(deck,2))
#p d=Marshal.dump(UpdateCommand.new(update,2))

#puts "load"
#p Marshal.load(c)
#p Marshal.load(d)
