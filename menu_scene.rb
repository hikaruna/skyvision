require 'socket'
class MenuScene < Scene::Base
	def init args
		@index = 0
    @deck_item = DeckItem.new(self, "デッキ選択")
		@items = [
			ServerItem.new(self, 'サーバーを立てる'),
			ClientItem.new(self, '接続する'),
			ClientHistoryItem.new(self, '履歴から接続する'),
      @deck_item,
			QuitItem.new(self, '休憩'),
		]
		@current_item = self
		@items_font_size = 20
		@items_font_margin = 30
		@fill_img = Image.new(1,@items_font_size,[96,255,255,255])
    @msg = nil
    @msg_count = 0

	end
	attr_accessor :current_item, :items, :deck_item
  def msg= msg
    @msg = msg
    @msg_count = 60
  end

	def update
		if @current_item == self
		  if Input.keyPush? K_UP
			  @index -= 1 if @index > 0
		  end
		  if Input.keyPush? K_DOWN
			  @index += 1 if @index < @items.size-1
		  end
			@current_item = @items[@index] if Input.keyPush? K_RETURN
			@index = @items.size-1 if Input.keyPush? K_ESCAPE
		else
			@current_item.update
	  end
	end

	def render
		@current_item.render_item
    if @msg
      Window.drawFramingFont(50,350,@msg,Font.get(64),[255,255,255],[0,0,0])
      @msg_count -= 1
      @msg = nil if @msg_count < 0
    end
    Window.drawFramingFont(500,10,$conf[:version].to_s,Font.get(12),[255,255,255],[0,0,0])
	end

	def render_item
		Window.drawFont(0,0, Window.fps.to_s, Font.get(60))
		Window.drawFont(13,13, "menu", Font.get(60))
		Window.drawScale(
			13,200+@index*(@items_font_size+@items_font_margin), @fill_img,
			Font.get(@items_font_size).getWidth(@items[@index].to_s),1,0,0)
		@items.each_with_index do |item, i|
			Window.drawFont(
				13,200+i*(@items_font_size+@items_font_margin),
				item.name, Font.get(@items_font_size))
		end
		Window.drawFont(320,150, @deck_item.deck[:name], Font.get(12))

		@deck_item.deck[:list].map do |i|
			str = "#{i}：#{Card.library[i]['Name'].chomp}"
			str.scan(/.{1,20}/).join("\n"+" "*8)
		end.each_slice(25).map do |i|
			i*"\n"
	  end.each_with_index do |name, i|
			Window.drawFont(320+i*160,180, name, Font.get(9))
		end

	end

	# デッキセレクトで決定ボタンが押されたとき
	def choise_deck
    user = @userlist[@userindex]
    @deck = loaddeck(user + "/" + @deckfile[user][@index])
    @index = 0
    @state = :active
	end
end

class Item
	def initialize parent, name
		@parent = parent
		@name = name
	end
	attr_reader :name
	def update
	end
	def render_item
	end
end
class GameStartItem < Item
	def update
		if Input.keyPush? K_ESCAPE
			@parent.current_item = @parent
		end
		if Input.keyPush? K_RETURN
			@host = IPSocket::getaddress(Socket::gethostname) 
			self.start
			user = $conf[:profile][:name]
			deck = @parent.deck_item.deck
			@parent.next_scene = GameScene.new(
				:master => @master, :table => @table, :id => @id, :deck => deck)
		end
		update_gamestart
  rescue
    @parent.msg = "通信に失敗しました"
    @recv_drb.stop_service if @recv_drb
	end
	def	update_gamestart
	end
	def start
	end
end
class ServerItem < GameStartItem
	def initialize parent, name
		super parent, name
		@index = 0
		@port = ("%05d"% $conf[:server_port]).to_s.split(//).map{|i|i.to_i}
	end
	def start
		$conf[:server_host] ||= $ip
		uri = URI.parse "druby://#{$conf[:server_host]}:#{@port.join('').to_i}"
    $conf[:uri]="server"
		@id = "#{uri.host}:#{uri.port}"
		@table = Table.new
    @table.members << {:name=>$conf[:profile][:name],:uri=>uri.to_s}
		@master = Master.new(@table)
		@recv_drb = DRb.start_service(uri.to_s, @master)
		puts "recv from #{uri}"
	end
	def update_gamestart
		if Input.keyPush?(K_UP)
			@port[@index]+=1
			@port[@index]=0 if @port[@index] > 9
		end
		if Input.keyPush?(K_DOWN)
			@port[@index]-=1
			@port[@index]=9 if @port[@index] < 0
		end
		if Input.keyPush?(K_LEFT)
			@index-=1 if @index > 0
		end
		if Input.keyPush?(K_RIGHT)
			@index+=1 if @index < @port.size-1
		end
	end
	def render_item
		@parent.render_item
		Window.drawFont(100,100, "Server", Font.get(40))
    Window.drawFont(100,150, "#{@port*''}\nOK?", Font.get(40))
    Window.fillRect(100+@index*20,150, 20,40, [96,255,255,255])
	end
end
module ClientItemModule
  def init
		@index = 0

		unless File.exist?($conf[:connect_history])
			File.open($conf[:connect_history],"w") do |f|
				YAML.dump([ "127.0.0.1:8901" ], f)
			end
		end

		@history = YAML.load_file($conf[:connect_history])
  end
	def start
		# urlの履歴の登録
		@history.delete(selected_url)
		@history.unshift(selected_url)
		File.open($conf[:connect_history], 'w'){|f| YAML.dump(@history, f) }
		# urlの履歴の登録ここまで

		target = URI.parse("druby://#{selected_url}").to_s
		@master = DRbObject.new_with_uri(target)
		@table = Table.new
		puts "connected to #{target}"

		uri = URI.parse "druby://0.0.0.0:#{$conf[:client_port]}"
		@id = "#{uri.host}:#{uri.port}"
		puts "try recv #{uri}"
		@recv_drb = DRb.start_service(uri.to_s, @table)
		uri = URI.parse "druby://#{$conf[:client_host]}:#{$conf[:client_port]}"
    $conf[:uri]=uri.to_s
		@master.connect( :name => $conf[:profile][:name], :uri => uri.to_s )
		puts "recv from #{uri}"
	end
end
class ClientHistoryItem < GameStartItem
  include ClientItemModule
  def initialize parent, name
		super parent, name
    init
    @target = @history.dup
    @index = 0
  end
  def selected_url
    @target[@index]
  end
  def render_item
    @parent.render_item
    Window.fillRect(100,100,
      Font.get(40).getWidth(@target[@index]),40, [0,0,0])
    Window.drawFont(100,100, @target[@index], Font.get(40))
  end
  def update_gamestart
    if Input.keyPush?(K_LEFT) || Input.keyPush?(K_UP)
      @index -= 1 if @index > 0
    elsif Input.keyPush?(K_RIGHT) || Input.keyPush?(K_DOWN)
      @index += 1 if @index < @history.size-1
    end
  end
end
class ClientItem < GameStartItem
  include ClientItemModule
	def initialize parent, name
		super parent, name
    init
		uri = URI.parse("dxruby://#{@history.first}")
    target = @history.first
    host_port = target.split(':')
    host = host_port[0].split('.').map{|i|"%03d"%i}
    port = "%05d"%host_port[1]
    uri_str = host*'' + port
    @target = uri_str.split(//).map{|i|i.to_i}
	end
  def selected_url
    get_url
  end
	def get_url
		ip_str = [
			[
				(@target[0..2]*'').to_i,
				(@target[3..5]*'').to_i,
				(@target[6..8]*'').to_i,
				(@target[9..11]*'').to_i,
		  ]*'.',
			(@target[12..16]*'').to_i.to_s
		]*':'
	end
	def update_gamestart
		if Input.keyPush?(K_UP)
			@target[@index]+=1
			@target[@index]=0 if @target[@index] > 9
		end
		if Input.keyPush?(K_DOWN)
			@target[@index]-=1
			@target[@index]=9 if @target[@index] < 0
		end
		if Input.keyPush?(K_LEFT)
			@index-=1 if @index > 0
		end
		if Input.keyPush?(K_RIGHT)
			@index+=1 if @index < @target.size-1
		end
	end
	def render_item
		@parent.render_item
		Window.fillRect(90,90,400,300,[196,0,0,0])
		Window.drawFont(100,100, "Client", Font.get(40))
		Window.drawFont(101,150,
		  "接続先：
			#{
			 [@target[0..2]*'',
			 @target[3..5]*'',
			 @target[6..8]*'',
			 @target[9..11]*'']*'.'
			}:#{@target[12..16]}\n
			あなたの待ち受けURI：
		#{$conf[:client_host]}:#{$conf[:client_port]}
			OK?", Font.get(20))
		Window.fillRect(100+@index*10+(@index/3*Font.get(20).getWidth('.')),
			170, 10, 20, [96,255,255,255])
	end
end
class DeckItem < Item
	def initialize parent, name
		super parent, name
		@decks = []
		@index = 0
		# デッキファイルリストの読み込み
    @decks = Dir::glob($conf[:deck]+'*').select{|path|File.file?(path)}
		choice
	end
	attr_reader :deck
	def update
		if Input.keyPush?(K_ESCAPE) || Input.keyPush?(K_RETURN)
			@parent.current_item = @parent
		end
		if Input.keyPush? K_LEFT
			@index-=1
		  @index = @decks.size-1 if @index < 0
			choice
		end
		if Input.keyPush? K_RIGHT
			@index+=1
		  @index = 0 if @index > @decks.size-1
			choice
		end
	end
	def render_item
		@parent.render_item
		Window.fillRect(320,100,300,450, [64, 255,255,255])
	end
	def choice
		deck = @decks[@index]
		cardlist = []
		File.open(deck,"r") do |f|
			f.each do |line|
				if line =~ /^[^,]+(,[^,]+)*/
				  maisu, no, name = line.split(",")
				  maisu.to_i.times { cardlist << no }
				end
			end
		end
		@deck =  { :name=>deck, :list=>cardlist[0...50]}
	end
end
class QuitItem < Item
	def update
		@parent.next_scene = Scene::Exit.new
	end
end
