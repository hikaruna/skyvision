class CardImg
  @@width  = $conf[:card_icon_width]
  @@height = $conf[:card_icon_height]
  @@res = $conf[:resource]
  @@img = {}
  @@bigimg = {}


  def self.reverse player_id=nil
    player_id = 0 if player_id.nil?
    @@reverse ||= Array.new(4) do |i|
      src = $conf[:reverse_img][i]
      if src.class == Array # カラー指定だった場合
        Image.new(
          @@width, @@height, [0,0,0]).draw(
            1,1,Image.new(
              @@width-2, @@height-2, $conf[:reverse_img][i]))
      elsif src.class == String && File.exist?(@@res+src)
        rt = RenderTarget.new(@@width,@@height)
        rt.fillRect(0,0,@@width,@@height, [0,0,0])
        rt.fillRect(1,1,@@width-2,@@height-2, [127,127,196])
        img = Image.load(@@res+src)
        rt.drawScale( 1,1,img,
          (@@width-2)/img.width.to_f,
          (@@height-2)/img.height.to_f,0,0)
        rt.update
        rt.to_image
      end
    end
    @@reverse[player_id]
  rescue
    p $conf[:reverse_color]
  end
  def self.[] key
    make(key) unless @@img[key]
    @@img[key]
  end
  def self.get_bigimg key
    make(key) unless @@bigimg[key]
    @@bigimg[key]
  end
  def self.make key
    unless Card.library[key]
      raise "#{key} ：そのようなNOのカードはありません"
    end
    rt = RenderTarget.new(@@width,@@height)
    path = "#{$conf[:resource]}cardillust/"
    if File.exist?(path+key+".png")
      filename = path+key+".png"
    elsif File.exist?(path+key+".jpg")
      filename = path+key+".jpg"
    end

    rt.fillRect(0,0,@@width,@@height, [0,0,0])
    rt.drawScale(1,1,$res[:card_bg],
      (@@width -2)/$res[:card_bg].width.to_f,
      (@@height-2)/$res[:card_bg].height.to_f,0,0)
    if filename
      @@bigimg[key] = Image.load(filename)
      rt.drawScale(1,1,@@bigimg[key],
        (@@width-2)/@@bigimg[key].width.to_f,
        (@@height-2)/@@bigimg[key].height.to_f,0,0)
    else
      @@bigimg[key] = Image.new(@@width,@@height)
      rt.drawFont(1,1,key,Font.get(10))
      Card.library[key]['Name'].split(/(.{4})/).grep(/.{1,}/).each_with_index do |s,i|
        rt.drawFont(1,11+i*7, s, Font.get(7))
      end
    end
    rt.update
    @@img[key] = rt.to_image
    puts "in car_img.make created no=>#{key.inspect}"
  end
end
