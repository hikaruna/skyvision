class Table
	def initialize
		@seed = Time.now
		@seednow = Time.now
	  @cards = []
	  @layer_cards = []
    p = { :life=>25, :menber=>nil }
		@players = [p.dup,p.dup,p.dup,p.dup]
		@commands = []
    @redo_stack = []
    @members = []
    @msg = nil
	end
	attr_accessor :cards, :layer_cards, :players, :commands, :msg, :members, :redo_stack

	def rev
		@commands.size
	end
	def exec command
		puts "called Table#exec(#{Marshal.load(command).inspect[0...80]})"
    raise unless command.instance_of? String
    command = Command.load(command)
		result = command.exec(self)
    if result
		  @commands << command
      @redo_stack = []
    end
		result
	end
  def undo i
    result = nil
    cmd = @commands.pop
    if cmd
		  result = cmd.revert(self)
      @redo_stack.insert(0, cmd)
    end
    result
  end
  def redo i
    result = nil
    cmd = @redo_stack.shift
    if cmd
		  result = cmd.exec(self)
		  @commands << cmd
    end
    result
  end
  def connect members
		puts "members=>#{members.inspect}"
    @members = members
  end
  def bye client
		puts "you are server. you are bye from #{client[:uri]}"
    @members.delete client
  end
  def join client
    puts "called Table#join"
    puts "in Table#join players-> #{@players.map{|p|p[:menber] ? p[:menber][:uri].inspect : p.inspect}}"
    puts "in Table#join client-> #{client[:uri].inspect}"
    puts "in Table#join players no included #{client[:uri]}" if @players.map{|p|p[:menber]}.include? client
    return nil if @players.map{|p|p[:menber]}.include? client
    i = @players.map{|p|p[:menber]}.index(nil)
    return nil unless i
    @players[i][:menber] = client
    i
  end
  def leave player_id
    @players[player_id][:menber]=nil
  end
end

class Master
	attr_reader :table
	def initialize table
		@table = table
		@distribute_tables = Hash.new
	end
  def copytable
    Marshal.dump(@table)
  end
	def exec command
    puts "called Master#exec !"
		result = @table.exec command
    puts "in Master#exec. start sync"
    puts "in Master#exec. target->#{@distribute_tables.inspect[0...80]}"
    replacate {|t| t.exec(command) }
    puts "in Master#exec. end sync"
		return result
  end
  def undo i=1
    puts "called Master#undo !"
		result = @table.undo i
    puts "in Master#undo masterUndo done !"
    puts "in Master#undo sync start for #{@distribute_tables.size}clients"
    replacate {|t| t.undo i }
    puts "in Master#undo sync end"
		return result
  end
  def redo i=1
		result = @table.redo i
    replacate {|t| t.redo i}
		return result
  end
  def replacate &b
		delete = []
		threads= {}
		@distribute_tables.each do |k,v|
			threads[k] = Thread.new(delete,v) do |tdelete, tv|
        yield tv
			end
		end
		threads.each{|k,v|
			begin
        v.join
	    rescue=>e
			  puts "in Master#replacate. sync error not sync to #{k.inspect[0...20]}"
			  delete << k
			end
    }

		delete.each do |i|
			puts "in Master#replacate. delete  #{delete} from dist_tables"
		  @distribute_tables.delete i
			puts "in Master#replacate. table.players->#{@table.players.map{|p|p ? p[:uri] : nil}.inspect}"
			puts "in Master#replacate. deleted client->#{i}"
      p_id = @table.players.index(@table.players.find{|p| p&&p[:uri] == i})
      if p_id
			  puts "in Master#replacate. deleted client is pleyer. leave #{delete}"
        leave(p_id)
      end
      @table.members.delete_if{|m|m&&m.include?(:uri)&&m[:uri]==i}
		end
	end
	def connect client
		puts "called Master#connect !"
		puts "from #{client.inspect}"
		dist_table = DRbObject.new_with_uri(client[:uri])
    @table.members << client
    replacate {|t| t.members << client}
    @table.commands.each do |cmd|
      dist_table.exec(cmd.dump)
    end
    dist_table.players = @table.players
    dist_table.redo_stack = @table.redo_stack
    dist_table.members = @table.members
		puts "in Master#connect. sync done #{client.inspect}"
		@distribute_tables[client[:uri]] = dist_table
	end
  def bye client
    table.bye client
		@distribute_tables.delete(client[:uri])
  end
  def join client
    id = table.join(client)
    replacate {|t| t.join(client) }
    id
  end
  def leave player_id
    table.leave player_id
    replacate {|t| t.leave(player_id) }
  end
end

