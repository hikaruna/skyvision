#! ruby -Ks
$KCODE= 'sjis'
$LOAD_PATH.unshift './lib/'
require 'yaml'
$conf = YAML.load_file("config.yml")
# メッセージ定義を読込
$conf[:message] = YAML.load_file($conf["message_path"])
require 'net/http'
require 'rexml/document'
require 'uri'
require 'socket' 
require 'edit_conf'
require 'drb/drb'
require 'pstore'

# vesion取得
str = "$Id$".gsub('$','').split(' ').values_at(3,4,2).insert(2,"Version:")
$conf[:version] = str.join(" ")


# グローバルIPを取得
begin
dist = 'checkip.dyndns.org'
html = Net::HTTP::get(dist,'/')
doc = REXML::Document.new(html)
yaml_str = doc.elements['/html/body'].text
yaml = YAML.load(yaml_str)
ip = yaml.values.first
$ip = ip
rescue => e
	raise e
  require 'dxruby'
	Window.loop do
	  Window.drawFont( 0,200,"デフォルト動作のIP自動取得に失敗しました。config.ymlにあなたのIPを設定してください。\n\nserver_host,\nclient_host\n\npush any key".scan(/.{1,20}/)*"\n", Font.new(30))
		break unless Input.keys.empty?
	end
  exit 1
end
# グローバルIPを取得 ここまで

require 'dxruby'
require 'scripts/scene'
require 'scripts/framing_font'
require 'scripts/misc'

$conf = key_to_hash($conf)
$conf[:server_host] ||= $ip
$conf[:client_host] ||= $ip


# メッセージをキーコードに
$conf[:message_code]= {:free => {}, :shift => {}}
$conf[:message][:free].each{|k,v|
  $conf[:message_code][:free][Input.const_get("K_#{k}")] = v
}
$conf[:message][:shift].each{|k,v|
  $conf[:message_code][:shift][Input.const_get("K_#{k}")] = v
}
# メッセージをキーコードに

# 画像のよみこみ
$res = Hash.new
$res[:reverse]= Image.new(
	$conf[:card_icon_width], $conf[:card_icon_height], [32,32,48])
$res[:card_bg] = Image.load($conf[:resource]+"card_bg.png")
# 画像のよみこみここまで

require 'card'
$conf[:profile][:name] ||= Card.library.values_at(*(Card.nums)).select{|c| c['Type'].chomp=="Character"}.choice['Name'].chomp

require 'card_img'
#require 'cardinfo'
require 'command'
require 'zone'
require 'table'

require 'menu_scene'
require 'game_controll'
require 'game_scene'

Window.width  = $conf[:window_width] || 640
Window.height = $conf[:window_height] || 480
Input.setRepeat(40,6)
begin
Scene.start MenuScene.new
rescue=> e
	File.open("error.log", "a") do |f|
		f.puts "\n\n#{Time.now}\n#{e.backtrace} (#{e.class.name})"
	end
	raise e
end
