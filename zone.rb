class Zone
  attr_accessor :x, :y, :w, :h, :c, :name
  def initialize name
    @x = $conf[:zone][name][:x]
    @y = $conf[:zone][name][:y]
    @w = $conf[:zone][name][:w]
    @h = $conf[:zone][name][:h]
    @c = [
      $conf[:zone][name][:r],
      $conf[:zone][name][:g],
      $conf[:zone][name][:b],]
    @name = name
  end
  def x1
    x  - w/2
  end
  def y1
    y  - h/2
  end
  def x2
    x  + w/2
  end
  def y2
    y  + h/2
  end
end
